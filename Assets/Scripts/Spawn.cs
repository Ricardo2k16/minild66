﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

    public GameObject agent;

    public Color teamColor;
    public string teamTag;

    public float distanciamento;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CreateTeam(int agents)
    {
        float dx;
        float dy;

        for( int i = 0; i < agents; i++)
        {
            dx = Random.value + distanciamento;
            dy = Random.value + distanciamento;

            GameObject novo = (GameObject)Instantiate(agent,
                new Vector3(transform.position.x + dx, transform.position.y + dy, transform.position.z),
                Quaternion.identity);

            novo.transform.tag = teamTag;
            novo.GetComponent<Renderer>().material.color = teamColor;
        }

    }
}
