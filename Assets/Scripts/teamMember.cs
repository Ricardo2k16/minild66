﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class teamMember : MonoBehaviour {

    private NavMeshAgent nma;
    private RaycastHit LoS;
    private gameManager gameManager;
    private bool isAttacking = false;

    public float accuarcy;
    public GameObject target;

	// Use this for initialization
	void Start () {
        nma = gameObject.GetComponent<NavMeshAgent>();
        gameManager = FindObjectOfType<gameManager>();     
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(transform.position, forward);

        Ray view = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(view, out LoS))
        {
            if(LoS.transform.gameObject == target.gameObject && isAttacking)
            {
                Destroy(LoS.transform.gameObject);
                isAttacking = false;
            }
        }

        if (gameManager.active == gameObject)
        {
            GetComponent<ParticleSystem>().startColor = new Color(0.5f, 0.5f, 0.5f, 0.75f);
            GetComponent<ParticleSystem>().Play();

            if (gameManager.passive != null)
            {
                nma.Stop();

                float step = Time.deltaTime * 100f;
                Vector3 relativePosition = gameManager.passive.transform.position - transform.position;
                transform.rotation = Quaternion.RotateTowards(transform.rotation,
                    Quaternion.LookRotation(relativePosition),
                    step);

                //transform.LookAt(gameManager.passive.transform);
            }
        }
        else if (gameManager.passive == gameObject)
        {
            GetComponent<ParticleSystem>().startColor = Color.red;
            GetComponent<ParticleSystem>().Play();
        }
        else
        {
            GetComponent<ParticleSystem>().Stop();
        }

    }

    void OnMouseUp()
    {
        gameManager.active = gameObject;
    }

    void OnMouseOver()
    {
        if (gameManager.active && gameManager.active != gameObject)
        {
            GetComponent<ParticleSystem>().startColor = Color.white;
            GetComponent<ParticleSystem>().Play();
        }

        if ( Input.GetMouseButtonDown(1) )
        {
            gameManager.active.GetComponent<teamMember>().target = gameObject;
            gameManager.active.GetComponent<teamMember>().isAttacking = true;
            gameManager.passive = gameObject;
        }        
    }

    public void setDestination(Vector3 destination)
    {
        nma.Resume();
        gameManager.passive = null;
        transform.LookAt(destination);
        nma.SetDestination(destination);
    }
}
