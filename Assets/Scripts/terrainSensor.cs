﻿using UnityEngine;
using System.Collections;

public class terrainSensor : MonoBehaviour {

    public GameObject toInstantiate;
    private gameManager gameManager;

    // Use this for initialization
    void Start () {
        gameManager = FindObjectOfType<gameManager>();
        //Instantiate<>
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    // Called only whem mouse is over the plane mesh. This way, we don't need to test if the plane
    // was hit on the click.
    void OnMouseOver()
    {
        // However, we want to know if the right mouse button was pressed. 
        if(Input.GetMouseButtonDown(1) && gameManager.active)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
                if( hit.transform.tag == "arena")
            {
                gameManager.active.GetComponent<teamMember>().setDestination(hit.point);
            }            
        }
    }
}
